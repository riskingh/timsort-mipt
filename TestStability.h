#ifndef __TimSort__TestStability__
#define __TimSort__TestStability__

#include <stdio.h>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <ctime>

#include "TimSort.h"

namespace test {
    class CStablePair {
    public:
        int value, index;
        CStablePair(int value = 0, int index = 0)
        : value(value), index(index) {}
    };
    bool operator < (const CStablePair &p1, const CStablePair &p2) {
        return p1.value < p2.value;
    }
    bool rightOrder(const CStablePair &p1, const CStablePair &p2) {
        return p1 < p2 || (p1.value == p2.value && p1.index < p2.index);
    }
    
    bool stabilityCheck(unsigned int tests = 100, unsigned int size = 1000, unsigned int mod = 10, bool showOnFalse = false) {
        bool isStable = true;
        std::vector<CStablePair> A(size);
        srand((int)time(0));
        int i;
        for (; tests > 0; --tests) {
            for (i = 0; i < size; ++i)
                A[i] = CStablePair(rand() % mod, i);
            timSort(A.begin(), A.end());

            for (i = 1; i < size; ++i)
                isStable &= rightOrder(A[i - 1], A[i]);
            if (showOnFalse && !isStable) {
                for (auto &elem : A)
                    std::cout << "(" << elem.value << ", " << elem.index << ") ";
                std::cout << "\n";
            }
        }
        return isStable;
    }
}

#endif /* defined(__TimSort__TestStability__) */
