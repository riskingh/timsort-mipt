#ifndef __TimSort_fast__TimSortParams__
#define __TimSort_fast__TimSortParams__

#include <stdio.h>

enum EMerge {
    NO_MERGE,
    MERGE_XY,
    MERGE_YZ
};

class TimSortParams {
public:
    virtual unsigned int getMinRun(unsigned int size) const = 0;
    virtual EMerge needMerge(unsigned int sizeX, unsigned int sizeY) const = 0;
    virtual EMerge needMerge(unsigned int sizeX, unsigned int sizeY, unsigned int sizeZ) const = 0;
    virtual bool needSprint(unsigned int inTheRow) const = 0;
};

#endif /* defined(__TimSort_fast__TimSortParams__) */
