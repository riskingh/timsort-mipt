#ifndef __TimSort_fast__TFunctions__
#define __TimSort_fast__TFunctions__

#include <stdio.h>
#include <vector>
#include <iostream>
#include <algorithm>
#include <ctime>
#include <string>
#include <cstdio>

#include "TimSort.h"

namespace test {
    template <class _T>
    bool isSorted(std::vector<_T> &v) {
        bool ans = true;
        for (typename std::vector<_T>::iterator it = v.begin() + 1; it != v.end(); ++it)
            ans &= !(*it < *(it - 1));
        return ans;
    }
    
    template <class _T>
    bool areEqual(std::vector<_T> &v1, std::vector<_T> &v2) {
        typename std::vector<_T>::iterator it, jt;
        for (it = v1.begin(), jt = v2.begin(); it != v1.end() && jt != v2.end() && *it == *jt; ++it, ++jt) {}
        return it == v1.end() && jt == v2.end();
    }
    
    template <class _PrintableIterator>
    void print(_PrintableIterator _first, _PrintableIterator _last) {
        for (; _first != _last; ++_first)
            std::cout << *_first << " ";
        std::cout << std::endl;
    }
    
    
    template <class _T>
    _T getRandom() {
        return _T();
    }
    
    template<> int getRandom<int>() {
        return rand();
    }

    template<> std::string getRandom<std::string>() {
        int size = 1000;
        std::string ans = "";
        for (; size > 0; --size)
            ans += (char)('a' + rand() % 26);
        return ans;
    }
    
    template <class _T>
    std::vector<_T> randomArray(unsigned int size, int swapCount = -1) {
        std::vector<_T> V(size);
        std::generate(V.begin(), V.end(), getRandom<_T>);
        if (swapCount >= 0) {
            std::sort(V.begin(), V.end());
            int first, second;
            for (; swapCount > 0; --swapCount) {
                first = rand() % size;
                second = rand() % size;
                std::swap(V[first], V[second]);
            }
        }
        return V;
    }

    template <class _RandomAccessIterator, class _Comparer = std::less<typename std::iterator_traits<_RandomAccessIterator>::value_type>>
    clock_t measureSort(void (*_sort)(const _RandomAccessIterator &, const _RandomAccessIterator &, _Comparer), _RandomAccessIterator _first, _RandomAccessIterator _last, _Comparer _comparer) {
        clock_t start = clock();
        _sort(_first, _last, _comparer);
        return clock() - start;
    }
    
    template <class _RandomAccessIterator, class _Comparer>
    clock_t measureSort(void (*_sort)(const _RandomAccessIterator &, const _RandomAccessIterator &, _Comparer, TimSortParams *), _RandomAccessIterator _first, _RandomAccessIterator _last, _Comparer _comparer, TimSortParams *params) {
        clock_t start = clock();
        _sort(_first, _last, _comparer, params);
        return clock() - start;
    }
    

    template <class _T, class _Comparer = std::less<_T>>
    std::pair<double, double> testRandom(unsigned int tests = 100, unsigned int size = 100000, int swapCount = -1, _Comparer _comparer = _Comparer(), TimSortParams *alternativeParams = NULL) {
        TimSortParams *defaultParams = new DefaultTimSortParams();
        std::vector<_T> A, B;
        unsigned int test;
        clock_t sumTim = 0, sumAlternativeSort = 0;
        int one = (tests + 19) / 20;
        std::cout << "---=---=---=---=---=\n";
        for (test = 0; test < tests; ++test) {
            A = randomArray<_T>(size, swapCount);
            B = std::vector<_T>(A.begin(), A.end());
            
            sumTim += measureSort(timSort, A.begin(), A.end(), std::less<_T>(), defaultParams);
            if (!alternativeParams)
                sumAlternativeSort += measureSort(std::sort, B.begin(), B.end(), _comparer);
            else
                sumAlternativeSort += measureSort(timSort, B.begin(), B.end(), _comparer, alternativeParams);
            
            if (!areEqual(A, B) || !isSorted(A)) {
                std::cerr << "\nERROR!\n";
                return std::make_pair(0, 0);
            }
            if (test % one == 0) {
                std::cout << "+";
                std::flush(std::cout);
            }
        }
        std::cout << std::endl;
        return std::make_pair((double)sumTim / (double)tests, (double)sumAlternativeSort / (double)tests);
    }
}
#endif /* defined(__TimSort_fast__TFunctions__) */
