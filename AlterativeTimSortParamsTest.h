#ifndef __TimSort__AlterativeTimSortParamsTest__
#define __TimSort__AlterativeTimSortParamsTest__

#include <stdio.h>
#include <ctime>

namespace test {
    class AlternativeTimSortParams: public TimSortParams {
    public:
        AlternativeTimSortParams() {}
        unsigned int getMinRun(unsigned int size) const;
        EMerge needMerge(unsigned int sizeX, unsigned int sizeY) const;
        EMerge needMerge(unsigned int sizeX, unsigned int sizeY, unsigned int sizeZ) const;
        bool needSprint(unsigned int inTheRow = 0) const;
    };

    unsigned int AlternativeTimSortParams::getMinRun(unsigned int size) const {
        return 60;
    }
    
    EMerge AlternativeTimSortParams::needMerge(unsigned int sizeX, unsigned int sizeY) const {
        return (sizeY <= sizeX) ? MERGE_XY : NO_MERGE;
    }

    EMerge AlternativeTimSortParams::needMerge(unsigned int sizeX, unsigned int sizeY, unsigned int sizeZ) const {
        if (sizeZ != 0 && sizeZ <= sizeY + sizeX)
            return (sizeX <= sizeZ) ? MERGE_XY : MERGE_YZ;
        return (sizeY <= sizeX) ? MERGE_XY : NO_MERGE;
    }
    
    bool AlternativeTimSortParams::needSprint(unsigned int inTheRow) const {
        return inTheRow > 3;
    }
}

#endif /* defined(__TimSort__AlterativeTimSortParamsTest__) */
