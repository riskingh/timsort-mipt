#ifndef __TimSort_fast__TimFunctions__
#define __TimSort_fast__TimFunctions__

#include <stdio.h>
#include <vector>

namespace NTim {
    
    template <class _RandomAccessIterator>
    inline typename std::iterator_traits<_RandomAccessIterator>::value_type previousValue(const _RandomAccessIterator &iter) {
        return *(iter - 1);
    }
    
    template <class _RandomAccessIterator>
    inline typename std::iterator_traits<_RandomAccessIterator>::value_type nextValue(const _RandomAccessIterator &iter) {
        return *(iter + 1);
    }
    
    
    template <class _RandomAccessIterator, class _Comparer>
    inline void insertionSort(_RandomAccessIterator &_first, _RandomAccessIterator &_last, _Comparer _comparer, unsigned int sortedSize = 0) {
        _RandomAccessIterator firstIterator, secondIterator;
        for (firstIterator = _first + sortedSize; firstIterator != _last; ++firstIterator) {
            for (secondIterator = firstIterator; secondIterator != _first && _comparer(*secondIterator, previousValue(secondIterator)); --secondIterator)
                std::swap(*(secondIterator - 1), *secondIterator);
        }
    }
    
    enum EOrder {
        DESCENDING = -1,
        UNOREDRED = 0,
        NOT_DESCENDING = 1
    };
    
    template <class _Type>
    inline EOrder getOrder(const _Type &t1, const _Type &t2) {
        return (t2 < t1) ? DESCENDING : NOT_DESCENDING;
    }
    
    template <class _Comparer>
    class CReverseComparer {
        /* "a" means anti. Constructor takes one argument - comparer. Returns
         value of comp(second, first), where first ans second - arguments. */
        _Comparer comparer;
    public:
        CReverseComparer(_Comparer _comparer)
        : comparer(_comparer) {}
        
        template <class _Type>
        bool operator ()(const _Type &t1, const _Type &t2) {
            return comparer(t2, t1);
        }
    };
    
    template <class _RandomAccessIterator>
    class CRun {
    private:
        _RandomAccessIterator first;
        unsigned int size;
    public:
        CRun(_RandomAccessIterator first, unsigned int size)
        : first(first), size(size) {}
        
        _RandomAccessIterator getFirst() const {
            return first;
        }
        _RandomAccessIterator getLast() const {
            return first + size;
        }
        unsigned int getSize() const {
            return size;
        }
        void attach(const CRun &run) {
            size += run.getSize();
        }
    };
    
    template <class _Type>
    class CRunStack {
    private:
        std::vector<_Type> data;
        
        _Type &at(int index) {
            return data[index + (int)data.size()];
        }
    public:
        CRunStack() {}
        
        unsigned int size() {
            return (unsigned int)data.size();
        }
        
        const _Type& operator [](int index) {
            return data[index + (int)data.size()];
        }
        void merge(int firstIndex) {
            at(firstIndex).attach(at(firstIndex + 1));
            if (firstIndex == -3)
                at(-2) = at(-1);
            data.pop_back();
        }
        void push(_Type t) {
            data.push_back(t);
        }
        void pop() {
            data.pop_back();
        }
    };

}

#endif /* defined(__TimSort_fast__TimFunctions__) */
