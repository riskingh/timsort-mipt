#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
#include <string>

#include "TestFunctions.h"
#include "TestStability.h"
#include "TimSort.h"
#include "AlterativeTimSortParamsTest.h"

void showTime(const std::pair<double, double> &tme) {
    double tmeTim, tmeAlt;
    tmeTim = (double)tme.first / CLOCKS_PER_SEC;
    tmeAlt = (double)tme.second / CLOCKS_PER_SEC;
    std::cout << "    TIM: " << tmeTim << std::endl;
    std::cout << "    ALT: " << tmeAlt << std::endl;
    std::cout << "TIM/ALT: " << tmeTim / tmeAlt << std::endl;
    std::cout << "ALT/TIM: " << tmeAlt / tmeTim << std::endl;
}

int main(int argc, const char * argv[]) {
    std::pair<double, double> tme(0, 0);
    std::string testType;
    std::cin >> testType;
    int testCount, arraySize, swapCount, mod;
    if (testType == "random") {
        std::cin >> testCount >> arraySize;
        tme = test::testRandom<int>(testCount, arraySize);
    }
    else if (testType == "sorted") {
        std::cin >> testCount >> arraySize >> swapCount;
        tme = test::testRandom<int>(testCount, arraySize, swapCount);
    }
    else if (testType == "stability") {
        std::cin >> arraySize >> mod;
        std::cout << "isStable: " << test::stabilityCheck(testCount, arraySize, mod) << "\n";
    }
    else if (testType == "random_strings") {
        std::cin >> testCount >> arraySize;
        tme = test::testRandom<std::string>(testCount, arraySize);
    }
    else if (testType == "sorted_strings") {
        std::cin >> testCount >> arraySize >> swapCount;
        tme = test::testRandom<std::string>(testCount, arraySize, swapCount);
    }
    else if (testType == "alt_params_random") {
        std::cin >> testCount >> arraySize;
        tme = test::testRandom<int>(testCount, arraySize, -1, std::less<int>(), new test::AlternativeTimSortParams());
    }
    else if (testType == "alt_params_sorted") {
        std::cin >> testCount >> arraySize >> swapCount;
        tme = test::testRandom<int>(testCount, arraySize, swapCount, std::less<int>(), new test::AlternativeTimSortParams());
    }
    else {
        std::cerr << "Unknown test type\n";
    }
    if (tme.first != -1)
        showTime(tme);
    return 0;
}
