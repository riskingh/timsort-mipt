#ifndef __TimSort_fast__TimSort__
#define __TimSort_fast__TimSort__

#include <stdio.h>
#include <vector>
#include <algorithm>
#include "TimFunctions.h"
#include "DefaultTimSortParams.h"

using namespace NTim;

class СTimSort {
private:
    const TimSortParams * const params;
    
    template <class _RandomAccessInputIterator, class _Type, class _RandomAccessOutputIterator, class _Comparer>
    void doSprint(_RandomAccessInputIterator &_first, const _RandomAccessInputIterator &_last, const _Type &_valueToCompareWith, _RandomAccessOutputIterator &_output, _Comparer _comparer) const {
        _RandomAccessInputIterator endOfCopying = std::lower_bound(_first, _last, _valueToCompareWith, _comparer);
        sprintTotal += endOfCopying - _first;
        _output = std::copy(_first, endOfCopying, _output);
        _first = endOfCopying;
    }
    
    template <class _RandomAccessIterator, class _Comparer>
    void mergePartsLR(const _RandomAccessIterator &_first, const _RandomAccessIterator &_middle, const _RandomAccessIterator &_last, _Comparer _comparer) const {
        bool lastOrder = false, currentOrder = false;
        unsigned int inTheRow = 0;
        std::vector<typename std::iterator_traits<_RandomAccessIterator>::value_type> tmp(_first, _middle);
        typename std::vector<typename std::iterator_traits<_RandomAccessIterator>::value_type>::iterator leftPartIterator = tmp.begin();
        _RandomAccessIterator rightPartIterator = _middle, resultIterator = _first;
        for (; leftPartIterator != tmp.end() && rightPartIterator != _last;) {
            currentOrder = _comparer(*rightPartIterator, *leftPartIterator);
            if (currentOrder)
                *resultIterator++ = *rightPartIterator++;
            else
                *resultIterator++ = *leftPartIterator++;
            if (lastOrder != currentOrder) {
                lastOrder = currentOrder;
                inTheRow = 0;
            }
            inTheRow++;
#ifndef NO_SPRINT
            if (params->needSprint(inTheRow) && leftPartIterator != tmp.end() && rightPartIterator != _last) {
                inTheRow = 0;
                if (!lastOrder && _comparer(*leftPartIterator, *rightPartIterator))
                    doSprint(leftPartIterator, tmp.end(), *rightPartIterator, resultIterator, _comparer);
                if (lastOrder && _comparer(*rightPartIterator, *leftPartIterator))
                    doSprint(rightPartIterator, _last, *leftPartIterator, resultIterator, _comparer);
            }
#endif
        }
        std::copy(leftPartIterator, tmp.end(), resultIterator);
    }
    
    template <class _RandomAccessIterator, class _Comparer>
    void mergeParts(const _RandomAccessIterator &_first, const _RandomAccessIterator &_middle, const _RandomAccessIterator &_last, _Comparer _comparer) const {
        if (_middle - _first <= _last - _middle)
            mergePartsLR(_first, _middle, _last, _comparer);
        else {
            typename std::vector< typename std::iterator_traits<_RandomAccessIterator>::value_type >::reverse_iterator rfirst(_first), rmiddle(_middle), rlast(_last);
            mergePartsLR(rlast, rmiddle, rfirst, CReverseComparer<_Comparer>(_comparer));
        }
    }
    
    template <class _RandomAccessIterator, class _Comparer>
    void mergeAndAttach(CRunStack<CRun<_RandomAccessIterator>> &runs, int firstIndex,  _Comparer _comparer) const {
        mergeParts(runs[firstIndex].getFirst(), runs[firstIndex + 1].getFirst(), runs[firstIndex + 1].getLast(), _comparer);
        runs.merge(firstIndex);
        mergeCount++;
    }
    
    template <class _RandomAccessIterator, class _Comparer>
    void mergeXY(CRunStack<CRun<_RandomAccessIterator>> &runs, _Comparer _comparer) const {
        mergeAndAttach(runs, -2, _comparer);
    }
    
    template <class _RandomAccessIterator, class _Comparer>
    void mergeYZ(CRunStack<CRun<_RandomAccessIterator>> &runs, _Comparer _comparer) const {
        mergeAndAttach(runs, -3, _comparer);
    }
    
public:
    mutable unsigned int mergeCount;
    mutable unsigned int sprintTotal;
    СTimSort(TimSortParams *params)
    : params(params), mergeCount(0), sprintTotal(0) {}
    
    template <class _RandomAccessIterator, class _Comparer = std::less<typename std::iterator_traits<_RandomAccessIterator>::value_type> >
    void sort(const _RandomAccessIterator &_first, const _RandomAccessIterator &_last, _Comparer _comparer = _Comparer()) const {
        CRunStack<CRun<_RandomAccessIterator>> runs;
        unsigned int minRun = params->getMinRun((unsigned int)(_last - _first)), x, y, z, sortedSize;
        EMerge mergeAction;
        EOrder order, newOrder;
        _RandomAccessIterator firstIterator, secondIterator;
        for (firstIterator = _first; firstIterator != _last; firstIterator = secondIterator) {
            if (firstIterator + 1 == _last)
                secondIterator = _last;
            else {
                sortedSize = 0;
                order = getOrder(*firstIterator, nextValue(firstIterator));
                secondIterator = firstIterator + 1;
                do {
                    newOrder = getOrder(previousValue(secondIterator), *secondIterator);
                    order = (order == newOrder) ? order : UNOREDRED;
                    sortedSize += (order != UNOREDRED);
                }
                while ((secondIterator - firstIterator < minRun || order != UNOREDRED) && ++secondIterator != _last);
                if (order == UNOREDRED) {
                    if (nextValue(firstIterator) < *firstIterator)
                        std::reverse(firstIterator, firstIterator + sortedSize);
                    insertionSort(firstIterator, secondIterator, _comparer, sortedSize);
                }
                if (order == DESCENDING)
                    std::reverse(firstIterator, secondIterator);
            }
            runs.push(CRun<_RandomAccessIterator>(firstIterator, (unsigned int)(secondIterator - firstIterator)));
            mergeAction = MERGE_XY;
            while (mergeAction != NO_MERGE && runs.size() > 1) {
                x = runs[-1].getSize();
                y = runs[-2].getSize();
                z = (runs.size() > 2) ? runs[-3].getSize() : 0;
                if (z > 0)
                    mergeAction = params->needMerge(x, y, z);
                else
                    mergeAction = params->needMerge(x, y);
                switch (mergeAction) {
                    case MERGE_XY:
                        mergeXY(runs, _comparer);
                        break;
                    case MERGE_YZ:
                        mergeYZ(runs, _comparer);
                        break;
                    case NO_MERGE:
                        break;
                }
            }
        }
        while (runs.size() > 1)
            mergeXY(runs, _comparer);
    }
};

template <class _RandomAccessIterator, class _Comparer>
void timSort(const _RandomAccessIterator &_first, const _RandomAccessIterator &_last, _Comparer _comparer, TimSortParams *_params) {
    class СTimSort T(_params);
    T.sort(_first, _last, _comparer);
}

template <class _RandomAccessIterator, class _Comparer = std::less<typename std::iterator_traits<_RandomAccessIterator>::value_type> >
void timSort(const _RandomAccessIterator &_first, const _RandomAccessIterator &_last, _Comparer _comparer = _Comparer()) {
    class СTimSort T(new DefaultTimSortParams());
    T.sort(_first, _last, _comparer);
}

#endif /* defined(__TimSort_fast__TimSort__) */
