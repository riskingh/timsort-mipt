#ifndef __TimSort_fast__DefaultTimSortParams__
#define __TimSort_fast__DefaultTimSortParams__

#include <stdio.h>
#include "TimSortParams.h"

class DefaultTimSortParams: public TimSortParams {
public:
    DefaultTimSortParams() {}
    unsigned int getMinRun(unsigned int size) const;
    EMerge needMerge(unsigned int sizeX, unsigned int sizeY) const;
    EMerge needMerge(unsigned int sizeX, unsigned int sizeY, unsigned int sizeZ) const;
    bool needSprint(unsigned int inTheRow = 0) const;
};

unsigned int DefaultTimSortParams::getMinRun(unsigned int size) const {
    bool isThere1 = false;
    for (; size > 64; size >>= 1)
        isThere1 |= size & 1;
    return size + (int)isThere1;
}

EMerge DefaultTimSortParams::needMerge(unsigned int sizeX, unsigned int sizeY) const {
    return (sizeY <= sizeX) ? MERGE_XY : NO_MERGE;
}

EMerge DefaultTimSortParams::needMerge(unsigned int sizeX, unsigned int sizeY, unsigned int sizeZ) const {
    if (sizeZ != 0 && sizeZ <= sizeY + sizeX)
        return (sizeX <= sizeZ) ? MERGE_XY : MERGE_YZ;
    return (sizeY <= sizeX) ? MERGE_XY : NO_MERGE;
}

bool DefaultTimSortParams::needSprint(unsigned int inTheRow) const {
    return inTheRow > 7;
}

#endif /* defined(__TimSort_fast__DefaultTimSortParams__) */
